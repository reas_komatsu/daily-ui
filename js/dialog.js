$('.dialog-open').on('click', function(){
    $('.dialog').addClass('dialog-display');
    console.log('オーバーレイ表示')
    
    $('.dialog-overlay').addClass('dialog-overlay-display');
    console.log('ポップアップ表示')
        
    $('.dialog-close, .dialog-overlay').off().click(function() {
        $('.dialog').removeClass('dialog-display');
        $('.dialog-overlay').removeClass('dialog-overlay-display');
        console.log('ポップアップ削除')
    });
});